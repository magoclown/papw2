@extends('main.master')

@section('header')
@parent
<br>
HEADER DEL HIJO
@endsection

@section('content')
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img src="https://cdnb.artstation.com/p/assets/images/images/024/014/895/medium/chun-lo-01-spider-man-promo.jpg?1581035627" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="https://cdnb.artstation.com/p/assets/images/images/024/014/895/medium/chun-lo-01-spider-man-promo.jpg?1581035627" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="https://cdnb.artstation.com/p/assets/images/images/024/014/895/medium/chun-lo-01-spider-man-promo.jpg?1581035627" class="d-block w-100" alt="...">
    </div>
  </div>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button>
  @component('components.alert')
    <strong>Whoops!</strong> Something went wrong!
    @endcomponent
    @component('components.popup')
        @slot('title')
        Titulo.
        @endslot
        @slot('body')
        Contenido
        @endslot
    @endcomponent
</div>
@endsection
